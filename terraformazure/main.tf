terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = "3.28.0"
    }
  }
}

provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "devopsrg" {
  name     = "devops-rg"
  location = "Central India"
}

resource "azurerm_virtual_network" "devopsrg" {
  name                = "devopsrg-network"
  location            = azurerm_resource_group.devopsrg.location
  resource_group_name = azurerm_resource_group.devopsrg.name
  address_space       = ["10.0.0.0/16"]
}

resource "azurerm_subnet" "devopsrg" {
  name                 = "internal"
  resource_group_name  = azurerm_resource_group.devopsrg.name
  virtual_network_name = azurerm_virtual_network.devopsrg.name
  address_prefixes     = ["10.0.2.0/24"]
}

resource "azurerm_public_ip" "devopsrg" {
  name                = "devops-publicip"
  location            = azurerm_resource_group.devopsrg.location
  resource_group_name = azurerm_resource_group.devopsrg.name

  allocation_method = "Dynamic"
}

resource "azurerm_network_interface" "devopsrg" {
  name                = "devopsrg-nic"
  location            = azurerm_resource_group.devopsrg.location
  resource_group_name = azurerm_resource_group.devopsrg.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.devopsrg.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id = azurerm_public_ip.devopsrg.id
  }
}

resource "azurerm_network_security_group" "devopsrg" {
  name                = "webserver"
  location            = azurerm_resource_group.devopsrg.location
  resource_group_name = azurerm_resource_group.devopsrg.name
  
  security_rule {
    access                     = "Allow"
    direction                  = "Inbound"
    name                       = "websrv"
    priority                   = 100
    protocol                   = "Tcp"
    source_port_range          = "*"
    source_address_prefix      = "*"
    destination_port_range     = "80"
    destination_address_prefix = "*"
  }
  security_rule {
  name                       = "SSH"
  priority                   = 101
  direction                  = "Inbound"
  access                     = "Allow"
  protocol                   = "Tcp"
  source_port_range          = "*"
  source_address_prefix      = "*"
  destination_port_range     = "22"
  destination_address_prefix = "*"

  }
}

resource "azurerm_subnet_network_security_group_association" "devopsrg" {
  subnet_id                 = azurerm_subnet.devopsrg.id
  network_security_group_id = azurerm_network_security_group.devopsrg.id
}

resource "azurerm_linux_virtual_machine" "devopsrg" {
  name                            = "vm1-devops"
  resource_group_name             = azurerm_resource_group.devopsrg.name
  location                        = azurerm_resource_group.devopsrg.location
  size                            = "Standard_F2"
  computer_name                   = "ubuntusrv"
  admin_username                  = "adminuser"
  admin_password                  = "qwe123QWE123"
  custom_data                     = filebase64("docker.sh")
  
  disable_password_authentication = false
  network_interface_ids = [ 
    azurerm_network_interface.devopsrg.id,
  ]
  
  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }

  os_disk {
    storage_account_type = "Standard_LRS"
    caching              = "ReadWrite"
  }

}
